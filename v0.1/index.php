<html>
	<head>
		<title>Simple ssh client</title>
		<script src="jq.js"></script>
		<script src="scr.js"></script>
		<link href="style.css" rel="stylesheet"></link>
	</head>
	<body id="console">
		<div id="status_bar">
			<b>Simple ssh client 0.1</b>
		</div>
		<pre id="cmd_res"></pre>
		<div id="buttons">
			<input id="cmd" type="text" placeholder="Enter command here..." />
			<button id="btn_send" onclick="send()">
				<img id='loading' src='/icon/progress.gif'/>
				<img id='ok' src='/icon/ok.png'/>
				<img id='warning' src='/icon/unknown.png'/>
				Enter
			</button>
			<a id='last_cmd' href=# onclick="$('#cmd').prop('value',$('#last_cmd').html());"></a>
		</div>
	</body>
	<script>
		show();
		document.getElementById("console").onkeydown = keyDown;
	</script>
</html>