function show(val = "none"){
	$("#buttons #btn_send #loading").fadeOut('fast');
	$("#buttons #btn_send #ok").fadeOut('fast');
	$("#buttons #btn_send #warning").fadeOut('fast');
	
	if(val != "none") $("#buttons #btn_send #"+val).fadeIn();
}

function send(){
	var cmd = document.getElementById("cmd").value;
	var log = document.getElementById("cmd_res");
	
	
	
	if(cmd){
		show('loading');
		$("#btn_send").prop("disabled", true);
		document.getElementById("cmd").value = "";
		$.post("ssh.php", { cmd: cmd })
			.done(function(data) {
				show();
				log.innerHTML = log.innerHTML + "\n" + data;
				log.scrollTop = log.scrollHeight - log.clientHeight;
				$("#btn_send").prop("disabled", false);
				$('#last_cmd').html(cmd)
			});
		
	}else{
		show("warning");
	}
}

function keyDown(e){
	if(e.keyCode == 13) send();
}